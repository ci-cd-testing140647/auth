import { TokenOptions } from '@lib/token';
import { DbOptions as GraphqlDbOptions } from '@graphql/plugin';
import { DbOptions as ServerDbOptions } from '@server/models';

import { FastifyListenOptions } from 'fastify';

export const config = {
  CONNECTION_STRING: 'tmp/db.json',
  JWT_PRIVATE_KEY: 'key',
  GRAPHQL_CONNECTION_STRING: 'tmp/db1.json',
  PORT: '80',
  HOST: '127.0.0.1',
  HASURA_CLOUD_ENDPOINT: 'https://accurate-sloth-79.hasura.app/v1/graphql',
  HASURA_ADMIN_SECRET:
    'jWJvCwm25nXA9Db2rMlfsOWlrOuTtpaPFuZNOVu1K9zoMW05yfHfXC0rkI62n93H',
  TEST_ENV: process.env.TEST_ENV || 'not found',
};

export const serverDbOptions: ServerDbOptions = {
  connectionString: config.HASURA_CLOUD_ENDPOINT,
  adminSecret: config.HASURA_ADMIN_SECRET,
};

export const graphqlDbOptions: GraphqlDbOptions = {
  connectionString: config.GRAPHQL_CONNECTION_STRING,
};

export const tokenOptions: TokenOptions = {
  key: config.JWT_PRIVATE_KEY,
};

export const fastifyServer: FastifyListenOptions = {
  port: parseInt(config.PORT),
  host: config.HOST,
};
